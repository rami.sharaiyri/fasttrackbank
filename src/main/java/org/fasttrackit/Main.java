package org.fasttrackit;

import java.io.InputStream;
import java.util.Scanner;

import static org.fasttrackit.AccountFactory.createAccount;
import static org.fasttrackit.View.*;

// Create user -> (user / password / Name / Surname / email / Phone) (/)
// Login user (/)
// Display IBAN and currency (view account details) (/)
// Display account balance (/)
// Create new account (/)
// Exchange rates
// Transfer between accounts
// SHIFT + HOME -> Select Line from start
// SHIFT + END -> Select line util the ned

public class Main {

    public static final int MAX_LOGIN_RETRY = 3;
    private static boolean authenticated = false;
    private static User user = null;
    private static String userOption = null;

    public static void main(String[] args) {
        System.out.println("    - = FastTrack Bank = -");
        System.out.println();
        InputStream in = System.in;
        Scanner keyboard = new Scanner(in);

        showWelcomingScreen();

        userOption = keyboard.nextLine();
        while (!userOption.equals(EXIT)) {
            runBankingApp(keyboard);
        }
        System.out.println("Good bye.");
    }

    private static void runBankingApp(Scanner keyboard) {
        if(!authenticated && userOption.equals("1")) {
            askUserForRegistrationDetails(keyboard);
            showNavigationMenu();
            return;
        }
        if(!authenticated && userOption.equals("2")) {
            user = askUserToLogin(keyboard);
        }
        if (!authenticated || user == null) {
            showContactSupport();
            return;
        }
        showNavigationMenu();
        userOption = keyboard.nextLine();
        Account defaultAccount = user.getDefaultAccount();
        if (userOption.equals(VIEW_ACCOUNT_DETAILS)) {
            showAccountDetails(defaultAccount);
            for (Account account : user.getAccounts()) {
                showAccountDetails(account);
            }
        }
        if (userOption.equals(CHECK_ACCOUNT_BALANCE)) {
            System.out.println("Account balance: " + defaultAccount.getBalance());
        }
        if (userOption.equals(CREATE_NEW_ACCOUNT)) {
            showSupportedCurrency();
            Account account = createAccount(keyboard.nextLine());
            if (account != null) {
                user.registerAccount(account);
                System.out.println("New account - IBAN: " + account.getIban() + " - Currency: " + account.getCurrency() + " has been created.");
            }
        }
    }

    private static User askUserToLogin(Scanner keyboard) {
        User bankUser = fetchUser();
        // if bank user is authenticated with credentials ...
        // display user and phoneNr

        for (int i = 0; i < MAX_LOGIN_RETRY; i++) {
            Credentials credentials = askforCredentials(keyboard);
            authenticated = bankUser.isAuthenticated(credentials);
            if (!authenticated) {
                System.out.println("Login id or password is incorrect.");
            }
            if (authenticated) {
                break;
            }
        }
        if (authenticated) {
            System.out.println();
            System.out.println("Welcome back mr. " + bankUser.getName());
            System.out.println();
            return bankUser;
        }
        return null;
    }

    private static Credentials askforCredentials(Scanner keyboard) {
        System.out.print("Please enter your login id: ");
        String loginId = keyboard.nextLine();
        System.out.print("Please enter your password: ");
        String password = keyboard.nextLine();
        return new Credentials(loginId, password);
    }

    private static User fetchUser() {
        String user = "rami.sh";
        String password = "12345";
        String name = "Sharaiyri";
        String surname = "Rami";
        String email = "ramy_ab@yahoo.com";
        String phoneNr = "0745700837";
        User bankUser = new User(user, password, name, surname);
        bankUser.setEmail(email);
        bankUser.setPhone(phoneNr);
        return bankUser;
    }
}